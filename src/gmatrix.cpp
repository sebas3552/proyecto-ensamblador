#include <cassert>
#include <cstdio>
#include <iostream>
#include <QImage>

#include "gmatrix.h"
#include "image.h"

using namespace std;

GMatrix::GMatrix(QObject* parent)
: QObject(parent)
{

}

GMatrix::~GMatrix()
{

}

int GMatrix::edit(Image* image, const int mod, const size_t* params)
{
    assert(image);
    assert(mod >= 0);
    int status = 0;
    if(image->changes == 0){
        //guarda el estado inicial de la imagen
        this->historial.push_back(*image);
        this->initialState = historial.begin();
        this->prevState = this->initialState;
        this->lastState = this->initialState;
    }
    switch(mod){
        case mode::TRIM:
            status = this->trim(image, params[0]);
        break;

        case mode::ROTATE:
            status = this->rotate(image, params[0]);
        break;

        case mode::BORDER:
            status = this->border(image, params[0], &params[1]);
        break;

        case mode::BW:
            status = this->bw(image);
        break;

        case mode::INVERT_COLOR:
            status = this->invertColor(image);
        break;

        case mode::BRIGHTNESS:
            status = this->brightness(image, params[0], params[1]);
        break;

        case mode::REVERSE:
            status = this->reverse(image);
        break;

        default:
            status = -1;
    }
    if(status != -1){
        //guarda el historial de cambios en la lista enlazada
        ++image->changes;
        this->historial.push_back(Image(*image));
        //situa el iterador de estado anterior sobre el último cambio realizado a la imagen
        ++this->prevState;
        ++this->lastState;
        assert(prevState != this->historial.end());
        image->saved = false;
        image->save();
        emit this->modified(image->changes);
    }
    return status;
}

int GMatrix::trim(Image* image, const size_t trimmedPixels)
{
    assert(image);
    //No es posible recortar más de la mitad de la imagen
    if(trimmedPixels >= image->width() || trimmedPixels >= image->height())
        return -1;
    //multiplicados por 2 porque se quitan de abajo y de arriba
    size_t newImageHeight = image->height() - (2 * trimmedPixels);
    size_t newImageWidth = image->width() - (2 * trimmedPixels);

    //pide memoria para la nueva matriz de pixeles recortada
    pixel_t** newImage = new pixel_t*[newImageHeight]();
    for(size_t row = 0; row < newImageHeight; ++row)
        newImage[row] = new pixel_t[newImageWidth]();
    //copia en la matriz nueva solo los pixeles dentro del rectángulo que dejó el recorte
    for(int row = (int) trimmedPixels; row < (int) (image->height() - trimmedPixels); ++row){
        asm volatile(
        "mov ecx, 0\n"
        "mov esi, %1\n"
        "mov edi, %0\n"
        "trimLoop:\n"
            "mov al, [esi]\n"
            "mov BYTE PTR[edi], al\n"
            "mov al, [esi+1]\n"
            "mov BYTE PTR[edi+1], al\n"
            "mov al, [esi+2]\n"
            "mov BYTE PTR[edi+2], al\n"
            "add esi, 3\n"
            "add edi, 3\n"
            "inc ecx\n"
            "cmp ecx, %2\n"
            "jl trimLoop\n"
        :
        : "r" (&newImage[row-trimmedPixels][0]), "r" (&image->pixels[row][trimmedPixels]), "g" (newImageWidth)
        : "%al", "%ecx"
        );
    }
    //libera la memoria de la matriz original
    image->deletePixels();
    //asigna la matriz nueva
    image->pixels = newImage;
    //modifica las dimensiones
    image->imageHeight = newImageHeight;
    image->imageWidth = newImageWidth;
    //intercambia las imágenes para hacer efectivo el recorte
    QImage img(newImageWidth, newImageHeight, QImage::Format_RGB888);
    image->thisImage.swap(img);
    return 0;
}

int GMatrix::rotate(Image* image, const size_t clockwise)
{
    assert(image);
    pixel_t** newImage = new pixel_t*[image->width()];
    int newImageHeight = image->width();
    int newImageWidth = image->height();
    if(clockwise){
        for(int row = 0; row < (int)image->width(); ++row){
            newImage[row] = new pixel_t[image->height()];
            for(int col = (int) image->height() - 1; col >= 0; --col){
                asm volatile(
                "mov esi, %0\n"
                "mov edi, %1\n"
                "mov al, [esi]\n"
                "mov BYTE PTR[edi], al\n"
                "mov al, [esi+1]\n"
                "mov BYTE PTR[edi+1], al\n"
                "mov al, [esi+2]\n"
                "mov BYTE PTR[edi+2], al\n"
                :
                : "r" (&image->pixels[col][row]), "r" (&newImage[row][image->height()-1-col])
                : "%al"
                );
            }
        }
    }else{
        for(int row = 0; row < (int)image->width(); ++row){
            newImage[row] = new pixel_t[image->height()];
            for(int col = 0; col < (int) image->height(); ++col){
                asm volatile(
                "mov esi, %0\n"
                "mov edi, %1\n"
                "mov al, [esi]\n"
                "mov BYTE PTR[edi], al\n"
                "mov al, [esi+1]\n"
                "mov BYTE PTR[edi+1], al\n"
                "mov al, [esi+2]\n"
                "mov BYTE PTR[edi+2], al\n"
                :
                : "r" (&image->pixels[col][image->width()-1-row]), "r" (&newImage[row][col])
                : "%al"
                );
            }
        }
    }
    image->deletePixels();
    image->imageHeight = newImageHeight;
    image->imageWidth = newImageWidth;
    image->pixels = newImage;
    QImage img(newImageWidth, newImageHeight, QImage::Format_RGB888);
    image->thisImage.swap(img);
    return 0;
}

int GMatrix::border(Image* image, const size_t thickness, const size_t *rgb)
{
    assert(image);
    assert(rgb);
    unsigned char R = (unsigned char) rgb[0];
    unsigned char G = (unsigned char) rgb[1];
    unsigned char B = (unsigned char) rgb[2];
    int cols = sizeof(pixel_t)*image->width();
    /*Borde superior*/
    for(int row = 0; row < (int) thickness; ++row){
        asm volatile(
        "mov ecx, %1\n"
        "mov esi, %0\n"
        "upperColsBorders:\n"
            "mov BYTE PTR[esi], %4\n"
            "mov BYTE PTR[esi+1], %3\n"
            "mov BYTE PTR[esi+2], %2\n"
            "add esi, 3\n"
            "sub ecx, 2\n"
            "loop upperColsBorders\n"
        :
        : "g" (&image->pixels[image->height() - 1 - row][0]), "g" (cols), "r" (R), "r" (G), "r" (B)
        : "%ecx"
        );
    }
    /*Borde inferior*/
    for(int row = 0; row < (int) thickness; ++row){
        asm volatile(
        "mov ecx, %1\n"
        "mov esi, %0\n"
        "lowerColsBorders:\n"
            "mov BYTE PTR[esi], %4\n"
            "mov BYTE PTR[esi+1], %3\n"
            "mov BYTE PTR[esi+2], %2\n"
            "add esi, 3\n"
            "sub ecx, 2\n"
            "loop lowerColsBorders\n"
        :
        : "g" (&image->pixels[row][0]), "g" (cols), "r" (R), "r" (G), "r" (B)
        : "%ecx"
        );
    }

    int borderThickness = thickness * sizeof(pixel_t);
    /*Borde izquierdo*/
    for(int row = 0; row < (int) image->height(); ++row){
        asm volatile(
        "mov ecx, %1\n"
        "mov esi, %0\n"
        "leftColsBorders:\n"
            "mov BYTE PTR[esi], %4\n"
            "mov BYTE PTR[esi+1], %3\n"
            "mov BYTE PTR[esi+2], %2\n"
            "add esi, 3\n"
            "sub ecx, 2\n"
            "loop leftColsBorders\n"
        :
        : "g" (&image->pixels[row][0]), "g" (borderThickness), "r" (R), "r" (G), "r" (B)
        : "%ecx"
        );
    }
    /*Borde derecho*/
    for(int row = 0; row < (int) image->height(); ++row){
        asm volatile(
        "mov ecx, %1\n"
        "mov esi, %0\n"
        "rightColsBorders:\n"
            "mov BYTE PTR[esi], %4\n"
            "mov BYTE PTR[esi+1], %3\n"
            "mov BYTE PTR[esi+2], %2\n"
            "sub esi, 3\n"
            "sub ecx, 2\n"
            "loop rightColsBorders\n"
        :
        : "g" (&image->pixels[row][image->width()-1]), "g" (borderThickness), "r" (R), "r" (G), "r" (B)
        : "%ecx"
        );
    }
    return 0;
}

int GMatrix::bw(Image* image)
{

    assert(image);
    unsigned char bn=' ';
    for(size_t row = 0; row < image->height(); ++row){
        for(size_t col = 0; col < image->width(); ++col){
            asm volatile(
                "mov esi, %0\n"
                "xor eax, eax\n"
                "mov %1, 0\n"
                "mov al, [esi]\n"
                "add %1, al\n"
                "mov al, [esi +1]\n"
                "add %1, al\n"
                "mov al, [esi +2]\n"
                "add %1, al\n"
                "mov al, %1\n"
                "mov bl, 3\n"
                "div bl\n"
                "mov BYTE PTR[esi], al\n"
                "mov BYTE PTR[esi+1], al\n"
                "mov BYTE PTR[esi+2], al\n"
            :
            : "g" (&(image->pixels[row][col])), "r" (bn)
            : "%al"
            );
        }
    }

    return 0;
}

int GMatrix::invertColor(Image* image)
{
    assert(image);

    int cols = image->width() * sizeof(pixel_t);

    for(size_t row = 0; row < image->height(); ++row){
        asm volatile(
        "mov esi, %0\n"
        "mov ecx, %1\n"
        "invertColors:\n"
            "mov al, 0xff\n"
            "mov bl, [esi]\n"
            "sub al, bl\n"
            "mov BYTE PTR[esi], al\n"
            "inc esi\n"
            "loop invertColors\n"
        "mov al, 0xff\n"
        :
        : "g" (&(image->pixels[row][0])), "g" (cols)
        : "%al", "%bl", "%ecx"
        );
    }
    return 0;
}

int GMatrix::brightness(Image* image, const size_t fact, const size_t increment)
{
    assert(image);
    unsigned char factor = (unsigned char) fact;

    if(increment){
        for(size_t row = 0; row < image->height(); ++row){
            for(size_t col = 0; col < image->width(); ++col){
                if(image->pixels[row][col].R + factor <= 255){
                    asm volatile(
                        "mov esi, %0\n"
                        "add BYTE PTR[esi+2], %1\n"
                    :
                    : "g" (&(image->pixels[row][col])), "r" (factor)
                    :
                    );
                }
                else{
                    asm volatile(
                        "mov esi, %0\n"
                        "mov BYTE PTR[esi+2], 255\n"
                    :
                    : "g" (&(image->pixels[row][col]))
                    :
                    );
                 }
                if(image->pixels[row][col].G + factor <= 255){
                    asm volatile(
                        "mov esi, %0\n"
                        "add BYTE PTR[esi+1], %1\n"
                    :
                    : "g" (&(image->pixels[row][col])), "r" (factor)
                    :
                    );
                }

                else{
                    asm volatile(
                        "mov esi, %0\n"
                        "mov BYTE PTR[esi+1], 255\n"
                    :
                    : "g" (&(image->pixels[row][col]))
                    :
                    );
                }
                if(image->pixels[row][col].B + factor <= 255){
                    asm volatile(
                        "mov esi, %0\n"
                        "add BYTE PTR[esi], %1\n"
                    :
                    : "g" (&(image->pixels[row][col])), "r" (factor)
                    :
                    );
                }

                else{
                    asm volatile(
                        "mov esi, %0\n"
                        "mov BYTE PTR[esi], 255\n"
                    :
                    : "g" (&(image->pixels[row][col])), "r" (factor)
                    :
                    );


                }
            }
        }
    }else{
        for(size_t row = 0; row < image->height(); ++row){
            for(size_t col = 0; col < image->width(); ++col){
                if((int)image->pixels[row][col].G >= factor){
                    asm volatile(
                        "mov esi, %0\n"
                        "sub BYTE PTR[esi+1], %1\n"
                    :
                    : "g" (&(image->pixels[row][col])), "r" (factor)
                    :
                    );
                }
                else{
                    asm volatile(
                        "mov esi, %0\n"
                        "mov BYTE PTR[esi+1], 0\n"
                    :
                    : "g" (&(image->pixels[row][col]))
                    :
                    );

                }


                if((int)image->pixels[row][col].B >= factor){
                    asm volatile(
                        "mov esi, %0\n"
                        "sub BYTE PTR[esi], %1\n"
                    :
                    : "g" (&(image->pixels[row][col])), "r" (factor)
                    :
                    );
                }

                else{
                    asm volatile(
                        "mov esi, %0\n"
                        "mov BYTE PTR[esi], 0\n"
                    :
                    : "g" (&(image->pixels[row][col]))
                    :
                    );
                }


                if((int)image->pixels[row][col].R >= factor){
                    asm volatile(
                        "mov esi, %0\n"
                        "sub BYTE PTR[esi+2], %1\n"
                    :
                    : "g" (&(image->pixels[row][col])), "r" (factor)
                    :
                    );
                }

                else{
                    asm volatile(
                        "mov esi, %0\n"
                        "mov BYTE PTR[esi+2], 0\n"
                    :
                    : "g" (&(image->pixels[row][col]))
                    :
                    );
                }

            }
        }
    }

    return 0;
}

int GMatrix::undo(Image* image)
{
    //si hay un estado anterior al actual, puede desplazarse hacia atrás
    if(image->changes > 0 && this->prevState != this->initialState){
        --this->prevState;
        //restaura la imagen al estado anterior, desplazando el iterador en la lista al elemento siguiente
        assert(*this->prevState != *image);
        *image = *this->prevState;
        image->saved = false;
        image->save();
    }else{
        image->saved = true;
    }
    emit modified(image->changes);
    return 0;
}

int GMatrix::redo(Image* image)
{
    //si hay un estado más reciente que el actual, puede desplazarse hacia adelante
    if(this->prevState != this->lastState){
        ++this->prevState;
        *image = *this->prevState;
        image->saved = false;
        image->save();
    }
    emit modified(image->changes);
    return 0;
}

int GMatrix::restore(Image* image)
{
    assert(image);
    *image = *this->initialState;
    image->save();
    image->saved = true;
    return 0;
}

void GMatrix::imageClosed()
{
    this->historial.clear();
    this->prevState = this->historial.end();
    this->lastState = this->prevState;
    this->initialState = this->lastState;
}

int GMatrix::reverse(Image* image)
{
    assert(image);
    int cols = image->width() * sizeof(pixel_t) / 2 - (image->width() * sizeof(pixel_t) % 2);
    for(size_t row = 0; row < image->height(); ++row){
        asm volatile(
        "mov ecx, %2\n"
        "mov ebx, %1\n"
        "mov esi, %0\n"
        "mov edi, 0\n"
        "reverseLoop:\n"
            "mov al, [esi]\n"
            "mov ah, [esi+1]\n"
            "mov dl, [esi+2]\n"
            "sub ebx, edi\n"
            "push ax\n"
            "push dx\n"
            "mov al, [ebx]\n"
            "mov ah, [ebx+1]\n"
            "mov dl, [ebx+2]\n"
            "mov BYTE PTR[esi], al\n"
            "mov BYTE PTR[esi+1], ah\n"
            "mov BYTE PTR[esi+2], dl\n"
            "pop dx\n"
            "pop ax\n"
            "mov BYTE PTR[ebx], al\n"
            "mov BYTE PTR[ebx+1], ah\n"
            "mov BYTE PTR[ebx+2], dl\n"
            "sub ecx, 2\n"
            "add esi, 3\n"
            "add edi, 3\n"
            "mov ebx, %1\n"
            "loop reverseLoop\n"
        :
        : "m" (&(image->pixels[row][0])), "g" (&(image->pixels[row][image->width() -1])), "g" (cols)
        : "%al", "%ah", "%ebx", "%ecx", "%dl", "%edi"
        );
    }
    return 0;
}
