#ifndef GMATRIX_H
#define GMATRIX_H

#include <cctype>
#include <list>
#include <QObject>

using namespace std;

/** Enumeración para representar las distintas operaciones a realizar en una imagen.
 *  En orden de aparición: recortar, rotar, bordes, blanco y negro, invertir color, brillo y reverso.
 */
enum mode
{
    TRIM,
    ROTATE,
    BORDER,
    BW,
    INVERT_COLOR,
    BRIGHTNESS,
    REVERSE
};

class Image;

/** @class GMatrix
 *  Esta clase representa el componente del modelo que modifica directamente las imágenes y emite señales de utilidad
 *  a la interfaz del usuario, cuando es necesario.
 */
class GMatrix : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(GMatrix)

    public:

        /// Constructor por defecto.
        explicit GMatrix(QObject* parent = nullptr);

        ///Destructor.
        virtual ~GMatrix();

        /**
         * @brief edit Subrutina que habilita la edición de imágenes dede su interfaz pública.
         * @param image Puntero a la imagen que se quiere modificar.
         * @param mod Constante entera que representa la modificación que se desea.
         * @param params Vector de parámetros, necesario para las funciones que requieren uno o más parámetros adicionales.
         * @return -1 si ocurrió algún error, 0 en caso contrario.
         */
        int edit(Image *image, const int mod, const size_t* params = nullptr);

        /**
         * @brief undo Subrutina que deshace un cambio a la imagen pasada por puntero.
         * @param image Puntero a la imagen a la que se desea deshacer un cambio, si es posible.
         * @return -1 si ocurrió algún error, 0 en caso contrario.
         */
        int undo(Image *image);

        /**
         * @brief redo Subrutina que rehace un cambio a la imagen pasada por parámetro.
         * @param image Puntero a la imagen a la que se desea rehacer un cambio, si es posible.
         * @return -1 si ocurrió algún error, 0 en caso contrario.
         */
        int redo(Image *image);

        /**
         * @brief restore Subrutina que restaura la imagen pasada por parámetro a su estado inicial.
         * @param image Puntero a la imagen a la que se desea restaurar desde su estado inicial.
         * @return -1 si ocurrió algún error, 0 en caso contrario.
         */
        int restore(Image *image);

     private:

        /**
         * @brief trim Subrutina que realiza el recorte de una imagen, cortando la misma cantidad
         * de pixeles de todos los bordes.
         * @param image Puntero a la imagen que se quiere recortar.
         * @param pixels Cantidad de pixeles a recortar.
         * @return -1 si ocurrió algún error, 0 en caso contrario.
         */
        int trim(Image* image, const size_t pixels);

        /**
         * @brief rotate Subrutina que realiza la rotación de una imagen, en ángulos factores de 90 grados.
         * @param image Puntero a la imagen que se quiere rotar.
         * @param clockwise Bandera que se establece en 1 si la rotación es en sentido horario, y 0 si es antihorario.
         * @return -1 si ocurrió algún error, 0 en caso contrario.
         */
        int rotate(Image* image, const size_t clockwise);

        /**
         * @brief border Subrutina que realiza el agregado de bordes planos monocromáticos a una imagen.
         * @param image Puntero a la imagen a la que se quiere añadir bordes.
         * @param thickness Grosor en pixeles del borde a añadir.
         * @param rgb Vector de componentes RGB que describen el color seleccionado para el borde.
         * @return -1 si ocurrió algún error, 0 en caso contrario.
         */
        int border(Image* image, const size_t thickness, const size_t *rgb);

        /**
         * @brief bw Subrutina que realiza la transformación de los colores de la imagen a escala de grises (blanco y negro).
         * @param image Puntero a la imagen que se desea transformar a blanco y negro.
         * @return -1 si ocurrió algún error, 0 en caso contrario.
         */
        int bw(Image* image);

        /**
         * @brief invertColor Subrutina que invierte los colores de una imagen.
         * @param image Puntero a la imagen a la que se quiere invertir el color.
         * @return -1 si ocurrió algún error, 0 en caso contrario.
         */
        int invertColor(Image* image);

        /**
         * @brief brightness Subrutina que realiza un cambio en el factor de brillo de la imagen.
         * @param image Puntero a la imagen a la que se desea modificar el factor de brillo.
         * @param factor Factor de brillo a modificar.
         * @param increment Bandera que se establece en 0 si se quiere hacer un decremento en el brillo, y en 1 si se
         * quiere hacer un incremento.
         * @return -1 si ocurrió algún error, 0 en caso contrario.
         */
        int brightness(Image* image, size_t fact, const size_t increment);

        /**
         * @brief mirror Subrutina que revierte la imagen.
         * @param image Puntero a la imagen que se desea revertir.
         * @return -1 si ocurrió algún error, 0 en caso contrario.
         */
        int reverse(Image* image);

        ///Lista de copias de objetos tipo Image, en la que se almacena todo el historial de ediciones de una imagen.
        list<Image> historial;

        ///Iteradores sobre el historial de ediciones, para avanzar y retrodecer entre estados de la imagen.
        list<Image>::iterator prevState, initialState, lastState;

     signals:
        /**
         * @brief modified Señal que se emite a MainWindow para que ésta tome acciones cada vez que se modifique una imagen.
         * @param changes Cantidad de cambios totales que se le han realizado a la imagen en cuestión.
         */
        void modified(int changes);

     private slots:
        /**
         * @brief imageClosed Receptáculo para el evento que ocasiona el cierre de una imagen.
         */
        void imageClosed();

};
#endif // GMATRIX_H
