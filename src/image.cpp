#include <cassert>
#include <cstring>
#include <exception>
#include <iostream>
#include <QColor>
#include <QImage>
#include <QPoint>
#include <QRgb>

#include "image.h"

using namespace std;

Image::Image(char *fileName)
: pixels(nullptr), changes(0), imageWidth(0), imageHeight(0), saved(true)
{
    if(fileName){
        this->name = strdup(fileName);
        this->thisImage = QImage(fileName);
    }
}

Image::Image(const Image &other)
: pixels(nullptr), name(nullptr), changes(other.changes), imageWidth(other.imageWidth), imageHeight(other.imageHeight), saved(other.saved)
{
    *this = other;
}

Image::~Image()
{
    this->deletePixels();
    if(this->name)
        free(this->name);
}

Image& Image::operator=(const Image &other)
{
    //evita autoasignación
    if(this == &other)
       return *this;
    delete this->name;
    this->name = strdup(other.name);
    this->changes = other.changes; 
    this->saved = other.saved;
    this->thisImage = other.thisImage;
    if(this->pixels)
        this->deletePixels();
    this->imageWidth = other.imageWidth;
    this->imageHeight = other.imageHeight;
    if(other.pixels){
        this->pixels = new pixel_t*[other.imageHeight];
        for(int row = 0; row < other.imageHeight; ++row){
            this->pixels[row] = new pixel_t[other.imageWidth];
            for(int column = 0; column < other.imageWidth; ++column)
                this->pixels[row][column] = other.pixels[row][column];   //copia cada struct pixel en la Imagen nueva
        }
    }
    return *this;
}

void Image::deletePixels()
{
    if(this->pixels)
        for(int row = 0; row < this->imageHeight; ++row)
            delete [] this->pixels[row];
    delete [] this->pixels;
    this->pixels = nullptr;
}

bool Image::operator==(Image& other)
{
    if(!strcmp(this->name, other.name))
        return false;

    if((this->height() != other.height()) || (this->width() != other.width()))
        return false;

    if((this->changes != other.changes) || (this->saved != other.saved))
        return false;

    for(size_t row = 0; row < this->height(); ++row){
        for(size_t col = 0; col < this->width(); ++col){
            if(this->pixels[row][col].B != other.pixels[row][col].B)
                return false;
            else if(this->pixels[row][col].G != other.pixels[row][col].G)
                return false;
            else if(this->pixels[row][col].R != other.pixels[row][col].R)
                return false;
        }
    }
    return true;
}

bool Image::operator!=(Image& other)
{
    return !(*this == other);
}

int Image::open()
{
    try{
        assert(!this->thisImage.isNull());
        QRgb pixel;
        this->pixels = new pixel_t*[this->thisImage.height()]();
        for(int i = 0; i < this->thisImage.height(); ++i){
            this->pixels[i] = new pixel_t[this->thisImage.width()]();
            for(int j = 0; j < this->thisImage.width(); ++j){
                pixel = this->thisImage.pixel(j, i);
                this->pixels[i][j].B = (unsigned char) qBlue(pixel);
                this->pixels[i][j].R = (unsigned char) qRed(pixel);
                this->pixels[i][j].G = (unsigned char) qGreen(pixel);
            }
        }
        this->imageWidth = this->thisImage.width();
        this->imageHeight = this->thisImage.height();
    }catch(exception &e){
        Q_UNUSED(e);
        return -1;
    }
    return 0;
}

int Image::save()
{
    assert(this->name);
    try{
        QColor color;
        for(int i = 0; i < this->imageHeight; ++i){
            for(int j = 0; j < this->imageWidth; ++j){
                color.setRgb((int) this->pixels[i][j].R, (int)this->pixels[i][j].G, (int)this->pixels[i][j].B);
                this->thisImage.setPixelColor(j, i, color);
            }
        }
        this->thisImage.save(this->name);
    }catch(exception &e){
        Q_UNUSED(e);
        return -1;
    }
    return 0;
}

size_t Image::height() const
{
    return this->imageHeight;
}

size_t Image::width() const
{
    return this->imageWidth;
}

char* Image::getName() const
{
    return this->name;
}

void Image::setName(const char *fileName)
{
    free(this->name);
    this->name = strdup(fileName);
}
