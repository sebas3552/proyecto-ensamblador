#ifndef IMAGE_H
#define IMAGE_H

class GMatrix;

class QImage;

///Estructura que representa un pixel, compuesto por sus tres componentes de color: R, G, B.
typedef struct pixel
{
    unsigned char B;
    unsigned char G;
    unsigned char R;
}pixel_t;

/**
 * @brief Clase Image
 * Mediante esta clase se modelan los archivos .bmp en un formato que facilita las operaciones a realizar.
 */
class Image
{
    ///Declaración de la clase GMatrix como amiga de Image, para que ésta pueda realizar modificaciones sobre sus atributos.
    friend class GMatrix;

	private:

        ///Matriz de pixeles que almacena la información de imagen visible del archivo .bmp.
        pixel_t** pixels;

        ///Nombre del archivo asociado a la imagen.
        char* name;

        ///Contador de cambios totales realizados a la imagen.
        int changes;

        ///Ancho de la imagen.
        int imageWidth;

        ///Alto de la imagen.
        int imageHeight;

        ///Representación de la imagen mediante un objeto QImage.
        QImage thisImage;

        ///Subrutina para desasignar la memoria de la matriz de pixeles.
        void deletePixels();

	public:

        ///Bandera que indica si la representación actual de la imagen está guardada en su archivo asociado.
        bool saved;

        /**
         * Constructor por omisión.
         * @param fileName nombre del archivo .bmp en el que se encuentra la imagen.
         */
        explicit Image(char *fileName = nullptr);

        /**
         * Constructor por copia.
         * @param other Imagen a copiar en la construcción del objeto apuntado por this.
         */
        Image(const Image& other);

        ///Destructor.
        ~Image();

        /**
         * @brief operator = Subrutina que habilita la sobrecarga del operador de asignación entre imágenes.
         * @param other Operando derecho de la asignación.
         * @return Objeto apuntado por this, por referencia.
         */
        Image& operator=(const Image & other);

        /**
         * @brief operator == Subrutina que habilita la sobrecarga del operador de igualdad entre imágenes.
         * @param other Operando derecho del operador de igualdad.
         * @return Verdadero si las imágenes son iguales, falso en caso contrario.
         */
        bool operator==(Image & other);

        /**
         * @brief operator =! Subrutina que habilita la sobrecarga del operador de diferencia entre imágenes.
         * @param other Operando derecho del operador de diferencia.
         * @return Verdadero si las imágenes son distintas, falso en caso contrario.
         */
        bool operator!=(Image & other);

        ///Subrutina que devuelve la altura, en pixeles, de la imagen.
        size_t height() const;

        ///Subrutina que devuelve el ancho, en pixeles, de la imagen.
        size_t width() const;

        /**
         * @brief open Subrutina que implementa las operaciones para abrir el archivo que contiene la imagen original.
         * Advertencia: No se debe tratar de modificar un objeto de esta clase sin antes haber invocado a esta subrutina.
         * @return -1 si no se pudo abrir el archivo indicado en el campo fileName, 0 en caso contrario.
         */
        int open();

        /**
         * @brief save Subrutina que guarda el estado actual de la imagen en su archivo asociado.
         * @return -1 si no se pudo guardar el archivo, 0 en caso contrario.
         */
        int save();

        ///Subrutina que devuelve el nombre del archivo asociado a la imagen.
        char* getName() const;

        ///Subrutina que establece el nombre del archivo asociado a la imagen.
        void setName(const char*);
};

#endif //IMAGE_H
