#include <cassert>
#include <cstdlib>
#include <iostream>
#include <QCloseEvent>
#include <QColorDialog>
#include <QDir>
#include <QFileDialog>
#include <QFileInfo>
#include <QGraphicsScene>
#include <QIcon>
#include <QInputDialog>
#include <QMessageBox>
#include <Qpoint>
#include <QRectF>
#include <QtGui>
#include <QToolButton>

#include "gmatrix.h"
#include "image.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
: QMainWindow(parent), ui(new Ui::MainWindow), image(nullptr), backSteps(0)
{
    ui->setupUi(this);
    this->scene = new QGraphicsScene(this);
    this->editor = new GMatrix();
    this->init();
    QObject::connect(this->editor, SIGNAL(modified(int)), this, SLOT(editedImage(int)));
    QObject::connect(this, SIGNAL(imageClosed(void)), this->editor, SLOT(imageClosed(void)));
}

MainWindow::~MainWindow()
{
    delete this->ui;
    delete this->scene;
    delete this->editor;
    if(this->image)
        delete this->image;
}

void MainWindow::on_pushButtonOpen_clicked()
{
    if(this->image)
        this->on_pushButtonClose_clicked();
    QString filter = "Mapa de bits (*.bmp) ;; Todos los archivos (*.*)";
    QString fileName = QFileDialog::getOpenFileName(this, "Abra un archivo", QDir::homePath(), filter);
    if(fileName.isEmpty())
        return;
    this->image = new Image(const_cast<char *>(qPrintable(fileName)));
    if(this->image->open() != -1){
        this->setButtons(true);
        this->reload();
    }else{
        this->errorMessage(QString("No se pudo abrir el archivo " + fileName));
    }
}

void MainWindow::closeEvent(QCloseEvent* event)
{
    Q_UNUSED(event);
    if(!this->image)
        return;
    if(!this->image->saved){
        const int choose = this->askUser("Guardar", "¿Desea guardar los cambios?");
        if(choose == this->YES){
            this->image->save();
        }else{
            if(choose == this->NO){
                this->editor->restore(this->image);
            }else{
                return;
            }
        }
    }
}

void MainWindow::on_pushButtonSave_clicked()
{
    if(!this->image){
        this->errorMessage(QString("No se puede guardar la imagen!"));
    }else{
        const int choose = this->askUser("Guardar", "¿Sobreescribir el archivo existente?");
        if(choose == this->YES){
            this->image->save();
            this->ui->pushButtonSave->setEnabled(false);
            this->image->saved = true;
        }
    }
}

void MainWindow::on_pushButtonBW_clicked()
{
    assert(this->image);
    this->editor->edit(this->image, mode::BW);
}

void MainWindow::reload()
{
    this->qp = QPixmap(this->image->getName());
    this->clearScreen();
    if(qp.isNull()){
        this->errorMessage(QString("No se pudo cargar la imagen!"));
        return;
    }else{
        this->scene->addPixmap(this->qp);
    }
    this->ui->view->setScene(this->scene);
    this->ui->view->fitInView(this->scene->sceneRect(), Qt::KeepAspectRatio);
}

void MainWindow::on_pushButtonBorder_clicked()
{
    int thickness = this->getUserInput("Borde", "Ingrese el tamaño del borde:");
    if(thickness == this->CANCEL || thickness == this->NO)
        return;
    QColor selectedColor = QColorDialog::getColor();
    if(!selectedColor.isValid())
        return;
    int R, G, B;
    selectedColor.getRgb(&R, &G, &B);
    if((thickness <= 0) || (thickness >= (int) this->image->height()) || (thickness >= (int) this->image->width())){
        this->errorMessage(QString("Entrada inválida! Debe ingresar una cantidad de pixeles válida"));
    }else{
        const size_t params[4] = {(size_t) thickness, (size_t) R, (size_t) G, (size_t) B};
        this->editor->edit(this->image, mode::BORDER, params);
    }
}

void MainWindow::on_pushButtonUndo_clicked()
{
    assert(this->image);
    this->editor->undo(this->image);
    ++this->backSteps;
}

void MainWindow::on_pushButtonRedo_clicked()
{
    assert(this->image);
    this->editor->redo(this->image);
    --this->backSteps;
}

void MainWindow::editedImage(int editions)
{
    if(editions > 0){
        this->ui->pushButtonUndo->setEnabled(true);
    }else{
        this->ui->pushButtonUndo->setEnabled(false);
    }

    if(this->image->saved){
        this->ui->pushButtonSave->setEnabled(false);
    }else{
        this->ui->pushButtonSave->setEnabled(true);
    }

    if(this->backSteps > 0){
        this->ui->pushButtonRedo->setEnabled(true);
    }else{
        this->ui->pushButtonRedo->setEnabled(false);
    }
    this->reload();
}

void MainWindow::on_pushButtonTrim_clicked()
{
    assert(this->image);
    int trimFactor = this->getUserInput(QString("Recortar"), QString("Ingrese la cantidad de pixeles a recortar: "));
    if(trimFactor == this->CANCEL || trimFactor == this->NO)
        return;
    if((trimFactor <= 0) || (trimFactor >= (int) (this->image->height() / 2)) || (trimFactor >= (int) (this->image->width() / 2))){
        this->errorMessage(QString("Entrada inválida! Debe ingresar una cantidad de pixeles válida"));
    }else{
        this->editor->edit(this->image, mode::TRIM, (size_t *) &trimFactor);
    }
}

int MainWindow::getUserInput(const QString title, const QString text)
{
    QString input = QInputDialog::getText(this, title, text);
    if(input.isNull())
        return 0;
    return atoi(qPrintable(input));
}

void MainWindow::errorMessage(const QString text)
{
    QMessageBox::information(this, "Error", text);
}

void MainWindow::setButtons(bool enabled, bool imageClosed)
{
    this->ui->pushButtonTrim->setEnabled(enabled);
    this->ui->pushButtonClose->setEnabled(enabled);
    this->ui->pushButtonBW->setEnabled(enabled);
    this->ui->pushButtonBorder->setEnabled(enabled);
    this->ui->pushButtonBrightness->setEnabled(enabled);
    this->ui->pushButtonInvColor->setEnabled(enabled);
    this->ui->pushButtonReverse->setEnabled(enabled);
    this->ui->toolButtonRotateLeft->setEnabled(enabled);
    this->ui->toolButtonRotateRight->setEnabled(enabled);
    this->ui->pushButtonSave->setEnabled(enabled && imageClosed);
    this->ui->pushButtonRedo->setEnabled(enabled && imageClosed);
    this->ui->pushButtonUndo->setEnabled(enabled && imageClosed);
}

void MainWindow::on_pushButtonInvColor_clicked()
{
    assert(this->image);
    this->editor->edit(this->image, mode::INVERT_COLOR);
}

void MainWindow::on_pushButtonBrightness_clicked()
{
    assert(this->image);
    int brightnessFactor = this->getUserInput(QString("Brillo"), QString("Ingrese el ajuste de brillo deseado: "));
    if(brightnessFactor == this->CANCEL || brightnessFactor == this->NO)
        return;
    if(brightnessFactor < 0){
        this->errorMessage(QString("Error! Debe ingresar un número válido"));
        return;
    }
    int increment = this->askUser("Brillo", "¿Incrementar brillo?");
    if(increment == this->CANCEL)
        return;
    size_t parameters[2] = {(size_t) brightnessFactor, (size_t) increment};
    this->editor->edit(this->image, mode::BRIGHTNESS, parameters);

}

void MainWindow::on_pushButtonClose_clicked()
{
    assert(this->image);
    if(!this->image->saved){
        const int choose = this->askUser(QString("Guardar"), QString("¿Desea guardar los cambios?"));
        if(choose == this->YES){
            this->image->save();
        }else{
            if(choose == this->NO){
                this->editor->restore(this->image);
            }else{
                return;
            }
        }
    }
    delete this->image;
    this->image = nullptr;
    this->clearScreen();
    emit this->imageClosed();
    this->setButtons(false, true);
}

int MainWindow::askUser(const QString title, const QString text)
{
    QMessageBox::StandardButton pressed = QMessageBox::question(this, title, text, QMessageBox::StandardButton::Yes | QMessageBox::StandardButton::No);
    if(pressed == QMessageBox::Yes)
        return this->YES;
    else if(pressed == QMessageBox::No)
        return this->NO;
    else
        return this->CANCEL;
}

void MainWindow::on_pushButtonReverse_clicked()
{
    assert(this->image);
    this->editor->edit(this->image, mode::REVERSE);
}

void MainWindow::on_toolButtonRotateLeft_clicked()
{
    assert(this->image);
    size_t clockwise = 0;
    this->editor->edit(this->image, mode::ROTATE, &clockwise);
}

void MainWindow::on_toolButtonRotateRight_clicked()
{
    assert(this->image);
    size_t clockwise = 1;
    this->editor->edit(this->image, mode::ROTATE, &clockwise);
}

void MainWindow::setButtonTooltip()
{
    this->ui->pushButtonOpen->setToolTip(QString("Abrir un archivo"));
    this->ui->pushButtonClose->setToolTip(QString("Cerrar la imagen actual"));
    this->ui->pushButtonSave->setToolTip(QString("Guardar la imagen actual"));
    this->ui->pushButtonUndo->setToolTip(QString("Deshacer"));
    this->ui->pushButtonRedo->setToolTip(QString("Rehacer"));
    this->ui->toolButtonRotateLeft->setToolTip(QString("Rotar la imagen 90 grados a la izquierda"));
    this->ui->toolButtonRotateRight->setToolTip(QString("Rotar la imagen 90 grados a la derecha"));
    this->ui->pushButtonBrightness->setToolTip(QString("Ajustar brillo"));
    this->ui->pushButtonBorder->setToolTip(QString("Configurar bordes"));
}

void MainWindow::setButtonIcons()
{
    this->ui->pushButtonUndo->setIcon(QIcon(":/icons/undo.png"));
    this->ui->pushButtonSave->setIcon(QIcon(":/icons/save.png"));
    this->ui->pushButtonRedo->setIcon(QIcon(":/icons/redo.jpg"));
    this->ui->toolButtonRotateLeft->setIcon(QIcon(":/icons/Rotate_left.png"));
    this->ui->toolButtonRotateRight->setIcon(QIcon(":/icons/Rotate_right.png"));
}

void MainWindow::init()
{
    this->setButtonIcons();
    this->setButtonTooltip();
    this->setButtons(false);
}

void MainWindow::clearScreen()
{
    this->scene->clear();
    this->ui->view->items().clear();
}
