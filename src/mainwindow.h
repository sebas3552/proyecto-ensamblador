#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QGraphicsPixmapItem>
#include <QGraphicsView>
#include <QMainWindow>

class GMatrix;
class Image;

namespace Ui {
class MainWindow;
}

/**
 * @brief clase MainWindow
 * Mediante esta clase se implementa la visualización de la imagen a editar, así como la interacción con el usuario
 * para que pueda elegir las operaciones a realizar sobre la imagen seleccionada.
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT
    Q_DISABLE_COPY(MainWindow)

    public:

        /**
         * Constructor por omisión.
         * @param parent Ventana padre.
         */
        explicit MainWindow(QWidget *parent = nullptr);

        ///Destructor.
        ~MainWindow();

    private slots:

        ///Recibidor de la señal de botón de abrir presionado.
        void on_pushButtonOpen_clicked();

        ///Recibidor de la señal de botón de guardar presionado.
        void on_pushButtonSave_clicked();

        ///Recibidor de la señal de botón de blanco y negro presionado.
        void on_pushButtonBW_clicked();

        ///Recibidor de la señal de botón de bordes presionado.
        void on_pushButtonBorder_clicked();

        ///Recibidor de la señal de botón de deshacer presionado.
        void on_pushButtonUndo_clicked();

        ///Recibidor de la señal de botón de rehacer presionado.
        void on_pushButtonRedo_clicked();

        ///Recibidor de la señal de botón de recortar presionado.
        void on_pushButtonTrim_clicked();

        ///Recibidor de la señal de botón de invertir color presionado.
        void on_pushButtonInvColor_clicked();

        ///Recibidor de la señal de botón de brillo presionado.
        void on_pushButtonBrightness_clicked();

        ///Recibidor de la señal de botón de cerrar presionado.
        void on_pushButtonClose_clicked();

        ///Recibidor de la señal de botón de reverso presionado.
        void on_pushButtonReverse_clicked();

        ///Recibidor de la señal de botón de rotación izquierda presionado.
        void on_toolButtonRotateLeft_clicked();

        ///Recibidor de la señal de botón de rotación derecha presionado.
        void on_toolButtonRotateRight_clicked();

        ///Recibidor de la señal que se envía cada vez que la imagen es editada.
        void editedImage(int);

private:

        ///Componente de interfaz de usuario.
        Ui::MainWindow *ui;

        ///Objeto para representación de gráficos.
        QPixmap qp;

        ///Objeto para representación de gráficos.
        QGraphicsScene* scene;

        ///Editor de imágenes con el que trabaja la ventana principal.
        GMatrix* editor;

        ///Imagen con la que trabaja la ventana principal. Se condicionó a una sola imagen a la vez, por simplicidad.
        Image* image;

        ///Constante simbólica para representar la selección del botón de "Aceptar".
        const int YES = 1;

        ///Constante simbólica para representar la selección del botón de "Rechazar".
        const int NO = 0;

        ///Constante simbólica para representar la selección del botón de "Cancelar" o cerrar ventana.
        const int CANCEL = -1;

        ///Contador de pasos retrocedidos mediante llamadas a GMatrix::redo().
        int backSteps;

        ///Recibidor de la señal de terminación de programa, para realizar tareas de finalización.
        void closeEvent(QCloseEvent*) override;

        /**
         * @brief errorMessage Subrutina que muestra un mensaje de error al usuario.
         * @param text Texto a ser mostrado.
         */
        void errorMessage(const QString text);

        /**
         * @brief setButtons Subrutina que modifica el estado de los botones de la interfaz según demanda.
         * @param enabled Bandera que se establece en 1 si los botones se deben activar, y en 0 si se deben desactivar.
         * @param imageClosed Bandera que se establece en 1 si se cerró la imagen, para realizar tareas adicionales.
         */
        void setButtons(bool enabled, bool imageClosed = false);

        /**
         * @brief askUser Subrutina que lanza un cuadro de decisión al usuario.
         * @param title Título de la ventana.
         * @param text Texto a ser mostrado.
         * @return constante simbólica que identifica la opción del usuario como SÍ, NO, CANCELAR.
         */
        int askUser(const QString title, const QString text);

        /**
         * @brief getUserInput Subrutina que lanza una petición de información al usuario.
         * @param title Título de la ventana.
         * @param text Texto a ser mostrado.
         * @return Entrada numérica del usuario.
         */
        int getUserInput(const QString title, const QString text);

        ///Subrutina para configurar los detalles de cada botón.
        void setButtonTooltip();

        ///Subrutina para configurar los íconos de los botones que lo necesiten.
        void setButtonIcons();

        ///Subrutina para inicializar la interfaz gráfica.
        void init();

        ///Subrutina que refresca la imagen en pantalla.
        void reload();

        ///Subrutina para limpiar el área de visualización de gráficos.
        void clearScreen();

    signals:

        ///Señal que se lanza cuando la imagen se cierra, a petición del usuario.
        void imageClosed();

};

#endif // MAINWINDOW_H
