/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.11.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QPushButton *pushButtonTrim;
    QPushButton *pushButtonBrightness;
    QPushButton *pushButtonOpen;
    QPushButton *pushButtonBW;
    QPushButton *pushButtonClose;
    QToolButton *toolButtonRotateLeft;
    QPushButton *pushButtonSave;
    QGraphicsView *view;
    QPushButton *pushButtonBorder;
    QPushButton *pushButtonReverse;
    QPushButton *pushButtonInvColor;
    QPushButton *pushButtonRedo;
    QPushButton *pushButtonUndo;
    QToolButton *toolButtonRotateRight;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(503, 300);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        centralWidget->setEnabled(true);
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        pushButtonTrim = new QPushButton(centralWidget);
        pushButtonTrim->setObjectName(QStringLiteral("pushButtonTrim"));

        gridLayout->addWidget(pushButtonTrim, 1, 1, 1, 1);

        pushButtonBrightness = new QPushButton(centralWidget);
        pushButtonBrightness->setObjectName(QStringLiteral("pushButtonBrightness"));

        gridLayout->addWidget(pushButtonBrightness, 1, 5, 1, 3);

        pushButtonOpen = new QPushButton(centralWidget);
        pushButtonOpen->setObjectName(QStringLiteral("pushButtonOpen"));

        gridLayout->addWidget(pushButtonOpen, 2, 0, 1, 1);

        pushButtonBW = new QPushButton(centralWidget);
        pushButtonBW->setObjectName(QStringLiteral("pushButtonBW"));

        gridLayout->addWidget(pushButtonBW, 1, 0, 1, 1);

        pushButtonClose = new QPushButton(centralWidget);
        pushButtonClose->setObjectName(QStringLiteral("pushButtonClose"));

        gridLayout->addWidget(pushButtonClose, 2, 2, 1, 1);

        toolButtonRotateLeft = new QToolButton(centralWidget);
        toolButtonRotateLeft->setObjectName(QStringLiteral("toolButtonRotateLeft"));

        gridLayout->addWidget(toolButtonRotateLeft, 2, 5, 1, 1);

        pushButtonSave = new QPushButton(centralWidget);
        pushButtonSave->setObjectName(QStringLiteral("pushButtonSave"));

        gridLayout->addWidget(pushButtonSave, 2, 1, 1, 1);

        view = new QGraphicsView(centralWidget);
        view->setObjectName(QStringLiteral("view"));
        view->setViewportUpdateMode(QGraphicsView::FullViewportUpdate);

        gridLayout->addWidget(view, 0, 0, 1, 8);

        pushButtonBorder = new QPushButton(centralWidget);
        pushButtonBorder->setObjectName(QStringLiteral("pushButtonBorder"));

        gridLayout->addWidget(pushButtonBorder, 1, 2, 1, 1);

        pushButtonReverse = new QPushButton(centralWidget);
        pushButtonReverse->setObjectName(QStringLiteral("pushButtonReverse"));

        gridLayout->addWidget(pushButtonReverse, 1, 3, 1, 1);

        pushButtonInvColor = new QPushButton(centralWidget);
        pushButtonInvColor->setObjectName(QStringLiteral("pushButtonInvColor"));

        gridLayout->addWidget(pushButtonInvColor, 1, 4, 1, 1);

        pushButtonRedo = new QPushButton(centralWidget);
        pushButtonRedo->setObjectName(QStringLiteral("pushButtonRedo"));

        gridLayout->addWidget(pushButtonRedo, 2, 4, 1, 1);

        pushButtonUndo = new QPushButton(centralWidget);
        pushButtonUndo->setObjectName(QStringLiteral("pushButtonUndo"));

        gridLayout->addWidget(pushButtonUndo, 2, 3, 1, 1);

        toolButtonRotateRight = new QToolButton(centralWidget);
        toolButtonRotateRight->setObjectName(QStringLiteral("toolButtonRotateRight"));

        gridLayout->addWidget(toolButtonRotateRight, 2, 7, 1, 1);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 503, 21));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Editor de im\303\241genes", nullptr));
        pushButtonTrim->setText(QApplication::translate("MainWindow", "Recortar", nullptr));
        pushButtonBrightness->setText(QApplication::translate("MainWindow", "Brillo", nullptr));
        pushButtonOpen->setText(QApplication::translate("MainWindow", "Abrir", nullptr));
        pushButtonBW->setText(QApplication::translate("MainWindow", "Blanco y Negro", nullptr));
        pushButtonClose->setText(QApplication::translate("MainWindow", "Cerrar", nullptr));
        toolButtonRotateLeft->setText(QString());
        pushButtonSave->setText(QApplication::translate("MainWindow", "Guardar", nullptr));
        pushButtonBorder->setText(QApplication::translate("MainWindow", "Borde", nullptr));
        pushButtonReverse->setText(QApplication::translate("MainWindow", "Espejo", nullptr));
        pushButtonInvColor->setText(QApplication::translate("MainWindow", "Invertir color", nullptr));
        pushButtonRedo->setText(QApplication::translate("MainWindow", "Rehacer", nullptr));
        pushButtonUndo->setText(QApplication::translate("MainWindow", "Deshacer", nullptr));
        toolButtonRotateRight->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
